Categories:Navigation
License:GPLv3
Web Site:http://brouter.de/brouter
Source Code:https://github.com/abrensch/brouter
Issue Tracker:https://github.com/abrensch/brouter/issues
Changelog:http://brouter.de/brouter/revisions.html

Auto Name:BRouter
Summary:Get elevation-aware cycling directions
Description:
Sophisticated routing engine for offline cycling directions. Works as a
selectable navigation service within a mapping application like
[[net.osmand.plus]]. Check out the [http://brouter.de/brouter/ website] for a
full description of features.
.

Repo Type:git
Repo:https://github.com/abrensch/brouter.git

Build:1.2,6
    commit=91c463302eccc47b9d2ada1104abaae948bd90aa
    subdir=brouter-routing-app
    maven=yes@..
    prebuild=sed -i -e '79,81d' pom.xml && \
        sed -i -e '44,70d' pom.xml

Build:1.3,7
    commit=13ac896b2f33f6d3bb33c37171488834dd9cfb0d
    subdir=brouter-routing-app
    maven=yes@..
    prebuild=sed -i -e '79,81d' pom.xml && \
        sed -i -e '44,70d' pom.xml

Build:1.3.1,8
    commit=850e8049fb9dbf5c83628406591108ea76379da8
    subdir=brouter-routing-app
    maven=yes@..
    prebuild=sed -i -e '79,81d' pom.xml && \
        sed -i -e '44,70d' pom.xml

Build:1.3.2,9
    commit=3a9c74a910cfe3853796817279610f1f0f241e46
    subdir=brouter-routing-app
    maven=yes@..
    prebuild=sed -i -e '79,81d' pom.xml && \
        sed -i -e '44,70d' pom.xml

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.3.2
Current Version Code:9
