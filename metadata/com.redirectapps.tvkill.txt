Categories:Multimedia,System
License:GPLv3
Web Site:
Source Code:https://github.com/42SK/TVKILL
Issue Tracker:https://github.com/42SK/TVKILL/issues

Auto Name:TV KILL
Summary:Turn off TVs
Description:
Turn off supported TVs using your device's built-in IR-blaster. The app basicly
transmits a variety of off-patterns for different TVs successively, which will
cause most TVs to power down.
.

Repo Type:git
Repo:https://github.com/42SK/TVKILL

Build:1.0.6,10
    commit=8f08b0f46a64fc04f238bedcb425df32f7a15551
    subdir=app
    gradle=yes

Build:1.0.7,11
    commit=v1.0.7
    subdir=app
    gradle=yes

Build:1.0.8,12
    commit=v1.0.8
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.8
Current Version Code:12
